<?php

declare(strict_types=1);


namespace Csoft\Collection;


use Csoft\Entity\Interfaces\PhotoInterface;
use Doctrine\Common\Collections\ArrayCollection;

class PhotoCollection extends ArrayCollection
{
    public function addPhoto(PhotoInterface $element): bool
    {
        return $this->add($element);
    }

    public function removePhoto(PhotoInterface $element)
    {
        return $this->removeElement($element);
    }

    /**
     * @return PhotoInterface[]
     */
    public function toArray(): array
    {
        return parent::toArray();
    }
}
