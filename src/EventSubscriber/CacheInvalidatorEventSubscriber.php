<?php

declare(strict_types=1);


namespace Csoft\EventSubscriber;


use Csoft\Entity\AbstractEntityWithImage;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class CacheInvalidatorEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EasyAdminEvents::POST_REMOVE  => ['invalidateCache'],
            EasyAdminEvents::POST_PERSIST => ['invalidateCache'],
            EasyAdminEvents::POST_UPDATE  => ['invalidateCache'],
        ];
    }

    public function invalidateCache(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        if (!($entity instanceof AbstractEntityWithImage)) {
            return;
        }

        $this->em->getRepository(get_class($entity))->invalidateCache();
    }
}
