<?php

declare(strict_types=1);


namespace Csoft\EventSubscriber;


use Csoft\Entity\AbstractEntityWithImage;
use Csoft\Service\Image\ImageService;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class ImageUploadEventSubscriber implements EventSubscriberInterface
{
    /** @var ImageService */
    private ImageService $imagesService;

    public function __construct(ImageService $imagesService)
    {
        $this->imagesService = $imagesService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EasyAdminEvents::PRE_REMOVE  => ['removeImage'],
            EasyAdminEvents::PRE_PERSIST => ['processImage'],
            EasyAdminEvents::PRE_UPDATE  => ['updateImage',],
        ];
    }

    public function removeImage(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        if (!($entity instanceof AbstractEntityWithImage)) {
            return;
        }

        $this->imagesService->removeFromDisk($entity);
    }

    public function hasNewImage(GenericEvent $event): bool
    {
        return $event->getSubject() instanceof AbstractEntityWithImage
            &&
            $event->getSubject()->getTempImageFileName() !== null;
    }

    public function updateImage(GenericEvent $event): void
    {
        if ($this->hasNewImage($event)) {
            $this->removeImage($event);
            $this->processImage($event);
        }
    }

    public function processImage(GenericEvent $event): void
    {
        $entity = $event->getSubject();
        $method = $event->getArgument('request')->getMethod();

        if (!($entity instanceof AbstractEntityWithImage) || $method !== Request::METHOD_POST) {
            return;
        }

        if ($this->hasNewImage($event)) {
            $url = $this->imagesService->saveToDisk(
                $this->imagesService->openImageFile($entity->getTempImageFileName(), true),
                $entity
            );

            $entity->setImageFileName($url);
        }
    }
}
