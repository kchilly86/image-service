<?php

declare(strict_types=1);


namespace Csoft\EventSubscriber;


use Csoft\Entity\AbstractEntityWithImage;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\Slugger\SluggerInterface;

class EntityWithImageEventSubscriber implements EventSubscriberInterface
{
    private SluggerInterface $slugger;

    public function __construct()
    {
        $this->slugger = new AsciiSlugger();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EasyAdminEvents::PRE_PERSIST => ['autoGenerateSlug'],
            EasyAdminEvents::PRE_UPDATE  => ['autoGenerateSlug',],
        ];
    }

    public function autoGenerateSlug(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        if (!($entity instanceof AbstractEntityWithImage)) {
            return;
        }

        // Only operate on empty friendly id.
        if (empty($entity->getSlug())) {
            $entity->setSlug(
                (string)$this->slugger->slug(
                    date('Ymd') . '-' .
                    $entity->getTitle()
                )->lower()
            );
        }
    }
}
