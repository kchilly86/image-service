<?php

declare(strict_types=1);


namespace Csoft\Entity\Interfaces;


interface PhotoInterface
{
    public function getId(): int;

    public function getPhotoAlbum(): PhotoAlbumInterface;

    public function setPhotoAlbum(PhotoAlbumInterface $photoAlbum): self;

    public function getImageFilePath(): ?string;

    public function setImageFilePath(string $imageFilePath): self;

    public function getImageWebPath(): ?string;

    public function setImageWebPath(string $imageWebPath): self;
}
