<?php

declare(strict_types=1);


namespace Csoft\Entity\Interfaces;


use Csoft\Collection\PhotoCollection;

interface PhotoAlbumInterface
{
    public function getPhotos(): ?PhotoCollection;

    public function setPhotos(?PhotoCollection $photos): self;
}
