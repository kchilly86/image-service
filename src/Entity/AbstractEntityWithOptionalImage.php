<?php

declare(strict_types=1);


namespace Csoft\Entity;


use ReflectionClass;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntityWithOptionalImage extends AbstractEntityWithImage
{
    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    protected $imageFileName;
}
