<?php

declare(strict_types=1);


namespace Csoft\Entity;


abstract class AbstractEntityWithOptionalOptimizedImage extends AbstractEntityWithOptionalImage implements EntityWithOptimizedImagesInterface
{
}
