<?php

declare(strict_types=1);


namespace Csoft\Entity;


class ImageResizeDetails
{
    private int $width;
    private int $height;
    private string $subFolder;
    private bool $isCropped;

    public function __construct(int $width, int $height, string $subFolder = 'thumbs', bool $isCropped = true)
    {
        $this->width = $width;
        $this->height = $height;
        $this->subFolder = $subFolder;
        $this->isCropped = $isCropped;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getSubFolder(): string
    {
        return $this->subFolder;
    }

    public function isCropped(): bool
    {
        return $this->isCropped;
    }
}
