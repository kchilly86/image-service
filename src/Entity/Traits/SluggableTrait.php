<?php

declare(strict_types=1);


namespace Csoft\Entity\Traits;


use Doctrine\ORM\Mapping as ORM;

trait SluggableTrait
{
    /**
     * @ORM\Column(type="string", length=80, nullable=false)
     */
    protected $slug;


    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
