<?php

declare(strict_types=1);


namespace Csoft\Entity\Traits;


use Doctrine\ORM\Mapping as ORM;

trait PhotoTrait
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $imageFilePath;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $imageWebPath;


    public function getId(): int
    {
        return $this->id;
    }

    public function getImageFilePath(): ?string
    {
        return $this->imageFilePath;
    }

    public function setImageFilePath(string $imageFilePath): self
    {
        $this->imageFilePath = $imageFilePath;

        return $this;
    }

    public function getImageWebPath(): ?string
    {
        return $this->imageWebPath;
    }

    public function setImageWebPath(string $imageWebPath): self
    {
        $this->imageWebPath = $imageWebPath;

        return $this;
    }
}
