<?php

declare(strict_types=1);


namespace Csoft\Entity;


use ReflectionClass;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntityWithOptimizedImage extends AbstractEntityWithImage implements EntityWithOptimizedImagesInterface
{
}
