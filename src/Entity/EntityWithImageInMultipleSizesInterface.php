<?php

declare(strict_types=1);


namespace Csoft\Entity;


interface EntityWithImageInMultipleSizesInterface
{
    /**
     * Returns the image resize version details.
     *
     * @return ImageResizeDetails[]
     */
    public function getImageResizeDetails(): array;
}
