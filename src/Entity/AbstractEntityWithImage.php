<?php

declare(strict_types=1);


namespace Csoft\Entity;


use Csoft\Entity\Traits\SluggableTrait;
use ReflectionClass;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntityWithImage
{
    use SluggableTrait;

    /**
     * @ORM\Column(type="string", length=120)
     */
    protected $imageFileName;

    protected $tempImageFileName;

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    public function getTempImageFileName(): ?string
    {
        return $this->tempImageFileName;
    }

    public function setTempImageFileName(string $tempImageFileName): self
    {
        $this->tempImageFileName = $tempImageFileName;

        return $this;
    }

    public function getImageFolderName(): string
    {
        return strtolower((new ReflectionClass($this))->getShortName());
    }
}
