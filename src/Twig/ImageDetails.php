<?php

declare(strict_types=1);


namespace Csoft\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ImageDetails extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('altPath', [$this, 'getAlternatePathFilter']),
        ];
    }

    public function getAlternatePathFilter(string $originalPath, string $thumbPath = 'thumbs'): string
    {
        return self::getAlternatePath($originalPath, $thumbPath);
    }

    public static function getAlternatePath(string $originalPath, string $thumbPath = 'thumbs'): string
    {
        $path = substr($originalPath, 0, strrpos($originalPath, '/'));
        $path .= '/' . $thumbPath;
        $path .= substr($originalPath, strrpos($originalPath, '/'));

        return $path;
    }
}
