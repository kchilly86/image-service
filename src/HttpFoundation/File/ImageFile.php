<?php

declare(strict_types=1);


namespace Csoft\HttpFoundation\File;


use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class ImageFile extends File
{
    private string $webPath;

    private array $validMimeTypes = [
        'image/png',
        'image/jpeg',
        'image/gif',
        'image/bmp',
        'image/svg',
        'image/svg+xml',
    ];

    private array $resizableMimeTypes = [
        'image/png',
        'image/jpeg',
        'image/gif',
        'image/bmp',
    ];

    public function __construct(string $path, bool $checkPath = true)
    {
        parent::__construct($path, $checkPath);

        $this->validate();
    }

    private function validate(): void
    {
        if (!in_array($this->getMimeType(), $this->validMimeTypes, true)) {
            throw new FileException('The requested file (' . $this->getMimeType() . ') is not a supported image file!');
        }
    }

    /**
     * @inheritDoc
     */
    public function move($directory, $name = null): self
    {
        $parent = parent::move($directory, $name);

        return new self((string)$parent, false);
    }

    /**
     * Copies the current file to the requested destination and returns the new file's instance.
     *
     * @param string $destination
     *
     * @return $this
     */
    public function copy(string $destination): self
    {
        $destinationFile = $destination . '/' . $this->getBasename();
        if (copy($this->getRealPath(), $destinationFile)) {
            return new self($destinationFile, false);
        }

        throw new FileException('The file cannot be copied to the destination!');
    }

    /**
     * @return string
     */
    public function getWebPath(): string
    {
        return $this->webPath;
    }

    /**
     * Sets the usable web link for the file.
     *
     * @param string $webPath
     *
     * @return ImageFile
     */
    public function setWebPath(string $webPath): self
    {
        $this->webPath = $webPath;

        return $this;
    }

    public function isResizable(): bool
    {
        return in_array($this->getMimeType(), $this->resizableMimeTypes, true);
    }
}
