<?php

declare(strict_types=1);


namespace Csoft\Repository;


use Csoft\Entity\AbstractEntityWithImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

abstract class CachedEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, $this->getEntityClassName());
    }

    abstract protected function getEntityClassName(): string;
    abstract protected function getEntityCachePrefix(): string;
    abstract protected function getEntityCacheTtl(): int;
    abstract protected function invalidateCache(): void;

    public function getCachedEntity(string $slug): AbstractEntityWithImage
    {
        return $this->createQueryBuilder('E')
            ->where('E.slug = :slug')
            ->setParameter(':slug', $slug)
            ->getQuery()
            ->setResultCacheId($this->getInstanceCacheKey($slug))
            ->setResultCacheLifetime($this->getEntityCacheTtl())
            ->getOneOrNullResult();
    }

    public function getInstanceCacheKey(string $id): string
    {
        return $this->getEntityCachePrefix() . $id;
    }

    private function getEntityInstanceKeys(): array
    {
        /** @var AbstractEntityWithImage[] $instances */
        $instances = $this->findAll();
        $keys = [];
        foreach ($instances as $instance) {
            $keys[] = $this->getInstanceCacheKey($instance->getSlug());
        }

        return $keys;
    }

    protected function invalidateCacheKeys(array $keys = []): void
    {
        $keys = array_merge($keys, $this->getEntityInstanceKeys());
        $resultCache = $this->_em->getConfiguration()->getResultCacheImpl();

        foreach ($keys as $current) {
            $resultCache->delete($current);
        }
    }
}
