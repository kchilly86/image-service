<?php

declare(strict_types=1);


namespace Csoft\Service\Image;


use Csoft\Entity\AbstractEntityWithImage;
use Csoft\Entity\AbstractEntityWithOptionalImage;
use Csoft\Entity\EntityWithImageInMultipleSizesInterface;
use Csoft\Entity\EntityWithOptimizedImagesInterface;
use Csoft\Entity\ImageResizeDetails;
use Csoft\HttpFoundation\File\ImageFile;
use Csoft\Service\Image\Optimizer\ImageOptimizerFactory;
use Csoft\Twig\ImageDetails;
use Symfony\Component\HttpKernel\KernelInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class ImageService
{
    private KernelInterface $kernel;

    private ImageOptimizerFactory $optimizerFactory;

    private LoggerInterface $logger;

    private string $entityImagePath;

    public function __construct(string $entityImagePath, KernelInterface $kernel, LoggerInterface $imageUploadLogger, ImageOptimizerFactory $optimizerFactory)
    {
        $this->kernel = $kernel;
        $this->optimizerFactory = $optimizerFactory;
        $this->logger = $imageUploadLogger;
        $this->entityImagePath = $this->sanitizeFolderName($entityImagePath ?? 'image');
    }

    public function sanitizeFolderName(string $path): string
    {
        return trim($path, '/\\');
    }

    public function sanitizePath(string $path): string
    {
        // Remove possible query string.
        $filteredPath = explode('?', $path)[0];

        // Remove trailing slash.
        return rtrim($filteredPath, '/\\');
    }

    public function openImageFile(string $path, bool $absolutePath = false): ImageFile
    {
        return $absolutePath
            ? new ImageFile($path)
            : new ImageFile(sprintf('%s/public/%s', $this->kernel->getProjectDir(), $this->sanitizePath($path)));
    }

    public function removeFromDisk(AbstractEntityWithImage $entity): void
    {
        if ($entity->getImageFileName() === null) {
            return;
        }

        $imageFile = $this->openImageFile($entity->getImageFileName());

        if ($entity instanceof EntityWithImageInMultipleSizesInterface) {
            foreach ($entity->getImageResizeDetails() as $resizeDetails) {
                $this->removeImageFile(
                    ImageDetails::getAlternatePath(
                        $imageFile->getRealPath(),
                        $resizeDetails->getSubFolder()
                    )
                );
            }
        }

        $this->removeImageFile($imageFile->getRealPath());
    }

    public function removeImageFile(string $path): void
    {
        $sanitizedPath = $this->sanitizePath($path);
        if (unlink($sanitizedPath)) {
            $this->logger->info(sprintf('The image file \'%s\' was deleted!', $sanitizedPath));
        } else {
            $this->logger->error(sprintf('The image file \'%s\' could not be deleted!', $sanitizedPath));
        }
    }

    public function saveToDisk(ImageFile $imageFile, AbstractEntityWithImage $entity): string
    {
        $optimizeMode = ($entity instanceof EntityWithOptimizedImagesInterface);
        $movedImage = $this->moveTempImageToEntityImage($imageFile, $entity);

        // Creating resized versions if it's necessary.
        if ($entity instanceof EntityWithImageInMultipleSizesInterface) {
            $this->createResizedVersions($movedImage, $entity, $optimizeMode);
        }

        // Optimizes the image if it's necessary.
        if (true ===$optimizeMode) {
            $this->optimizeImage($movedImage);
        }

        return $movedImage->getWebPath();
    }

    /**
     * Moves the temp image under the entity image folder and returns the destination image file instance.
     *
     * @param ImageFile $imageFile
     * @param AbstractEntityWithImage $entity
     *
     * @return ImageFile
     */
    protected function moveTempImageToEntityImage(ImageFile $imageFile, AbstractEntityWithImage $entity): ImageFile
    {
        $destinationFileName = $entity->getSlug() . '.' . $imageFile->guessExtension();
        $destinationRelativePath = sprintf('%s/%s', $this->entityImagePath, $entity->getImageFolderName());
        $destinationAbsolutePath = sprintf(
            '%s/public/%s',
            $this->kernel->getProjectDir(),
            $destinationRelativePath
        );

        $movedImage = $imageFile->move($destinationAbsolutePath, $destinationFileName);
        $movedImage->setWebPath(sprintf(
            '/%s/%s?v=%s',
            $destinationRelativePath,
            $destinationFileName,
            $this->generateWebVersionString()
        ));

        $this->logger->info(
            sprintf('The uploaded file got moved to \'%s\'!', $movedImage->getRealPath())
        );

        return $movedImage;
    }

    protected function generateWebVersionString(): string
    {
        return hash('crc32b', uniqid('webVersion', true));
    }

    protected function createResizedVersions(ImageFile $imageFile, EntityWithImageInMultipleSizesInterface $entity, bool $optimizeMode): void
    {
        foreach ($entity->getImageResizeDetails() as $resizeDetail) {
            $this->createResizedVersion($imageFile, $resizeDetail, $optimizeMode);
        }
    }

    public function createResizedVersion(ImageFile $imageFile, ImageResizeDetails $resizeDetails, bool $optimizeMode): void
    {
        if ($imageFile->isResizable()) {
            $process = new Process([
                sprintf(
                    '%s/vendor/bin/createResizedImage',
                    $this->kernel->getProjectDir(),
                ),
                $resizeDetails->isCropped() ? 'crop' : 'resize',
                $imageFile->getRealPath(),
                $imageFile->getPath() . '/' . $resizeDetails->getSubFolder(),
                $resizeDetails->getHeight() === 0 ? '' : $resizeDetails->getHeight(),
                $resizeDetails->getWidth() === 0 ? '' : $resizeDetails->getWidth(),
            ]);

            $this->logger->info('Creating alternate image version: ' . $process->getCommandLine());
            $process->run(function ($type, $buffer) {
                if (Process::ERR === $type) {
                    $this->logger->error('Error: ' . $buffer);
                } else {
                    $this->logger->info($buffer);
                }
            });

            $resizedImageFile = $this->openImageFile(
                ImageDetails::getAlternatePath($imageFile->getRealPath(), $resizeDetails->getSubFolder()),
                true
            );
        } else {
            $resizedImageFile = $imageFile->copy($imageFile->getPath() . '/' . $resizeDetails->getSubFolder());
        }

        if ($optimizeMode) {
            $this->optimizeImage($resizedImageFile);
        }
    }

    /**
     * Optimizes the given image file in place.
     *
     * @param ImageFile $imageFile
     */
    public function optimizeImage(ImageFile $imageFile): void
    {
        $optimizer = $this->optimizerFactory->getOptimizerInstance($imageFile);

        if ($optimizer !== null) {
            $this->logger->info($optimizer->optimize($imageFile));
        }
    }
}
