<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;
use Symfony\Component\Process\Process;

abstract class AbstractImageOptimizer implements ImageOptimizerInterface
{
    /**
     * Returns the optimizer command in an array.
     *
     * @return array
     */
    abstract protected function getOptimizerCommands(): array;

    /**
     * @inheritDoc
     */
    public function optimize(ImageFile $imageFile): string
    {
        $process = new Process(array_merge(
            $this->getOptimizerCommands(),
            [$imageFile->getRealPath()]
        ));

        ob_start();
        echo 'Image optimization: ' . $process->getCommandLine() . PHP_EOL;
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'Error: ' . $buffer . PHP_EOL;
            } else {
                echo $buffer . PHP_EOL;
            }
        });

        return ob_get_clean();
    }
}
