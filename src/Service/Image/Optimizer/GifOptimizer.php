<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;

class GifOptimizer extends AbstractImageOptimizer
{
    /**
     * @inheritDoc
     */
    public function isApplicable(ImageFile $imageFile): bool
    {
        return $imageFile->getMimeType() === 'image/gif';
    }

    /**
     * @inheritDoc
     */
    protected function getOptimizerCommands(): array
    {
        return [
            'gifsicle',
            '--batch',
            '--optimize',
        ];
    }
}
