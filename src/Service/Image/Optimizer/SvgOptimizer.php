<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;
use Symfony\Component\HttpKernel\KernelInterface;

class SvgOptimizer extends AbstractImageOptimizer
{
    /**
     * @var KernelInterface
     */
    private KernelInterface $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @inheritDoc
     */
    public function isApplicable(ImageFile $imageFile): bool
    {
        return in_array($imageFile->getMimeType(), ['image/svg+xml', 'image/svg+xml'], true);
    }

    /**
     * @inheritDoc
     */
    protected function getOptimizerCommands(): array
    {
        return [
            $this->kernel->getProjectDir() . '/node_modules/svgo/bin/svgo',
        ];
    }
}
