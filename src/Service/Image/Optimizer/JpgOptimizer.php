<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;
use ErrorException;
use Symfony\Component\Process\Process;

class JpgOptimizer extends AbstractImageOptimizerWithQuality
{
    /**
     * @inheritDoc
     */
    protected function getDefaultQuality(): string
    {
        return '80';
    }

    /**
     * @inheritDoc
     */
    protected function validateQuality(): void
    {
        $imageQuality = intval($this->imageQuality);
        if ($imageQuality < 1 || $imageQuality > 100) {
            throw new ErrorException('Image quality needs to be between 1-100!');
        }
    }

    /**
     * @inheritDoc
     */
    public function isApplicable(ImageFile $imageFile): bool
    {
        return $imageFile->getMimeType() === 'image/jpeg';
    }

    /**
     * @inheritDoc
     */
    protected function getOptimizerCommands(): array
    {
        return [
            'jpegoptim',
            '--max=' . $this->imageQuality,
        ];
    }
}
