<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;

class ImageOptimizerFactory
{
    /** @var ImageOptimizerInterface[] */
    private $optimizers;

    /**
     * ImageOptimizerFactory constructor.
     *
     * @param ImageOptimizerInterface[] $optimizers
     */
    public function __construct(iterable $optimizers)
    {
        $this->optimizers = $optimizers;
    }

    /**
     * Returns the applicable image optimizer instance.
     *
     * @param ImageFile $imageFile
     *
     * @return ImageOptimizerInterface|null
     */
    public function getOptimizerInstance(ImageFile $imageFile): ?ImageOptimizerInterface
    {
        foreach ($this->optimizers as $optimizer) {
            if ($optimizer->isApplicable($imageFile)) {
                return $optimizer;
            }
        }

        return null;
    }
}
