<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;
use ErrorException;
use Symfony\Component\Process\Process;

abstract class AbstractImageOptimizerWithQuality extends AbstractImageOptimizer
{
    protected $imageQuality;

    public function __construct($imageQuality = null)
    {
        $this->imageQuality = $imageQuality ?? $this->getDefaultQuality();
        $this->validateQuality();
    }

    /**
     * Returns the default quality.
     *
     * @return string
     */
    abstract protected function getDefaultQuality(): string;

    /**
     * Throws and error on validation error!
     *
     * @throws ErrorException
     */
    abstract protected function validateQuality(): void;
}
