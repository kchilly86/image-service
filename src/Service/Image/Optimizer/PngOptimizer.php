<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;
use ErrorException;

class PngOptimizer extends AbstractImageOptimizerWithQuality
{
    /**
     * @inheritDoc
     */
    protected function getDefaultQuality(): string
    {
        return '65-80';
    }

    /**
     * @inheritDoc
     */
    protected function validateQuality(): void
    {
        $imageQualities = (array)explode('-', $this->imageQuality);
        if (count($imageQualities) > 2) {
            throw new ErrorException('Image quality range needs to contain two numbers! [for example: 60-85]');
        }

        foreach ($imageQualities as $imageQuality) {
            if ($imageQuality < 1 || $imageQuality > 100) {
                throw new ErrorException('Image quality needs to be between 1-100 or range! [for example: 85 or 60-85]');
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function isApplicable(ImageFile $imageFile): bool
    {
        return $imageFile->getMimeType() === 'image/png';
    }

    /**
     * @inheritDoc
     */
    protected function getOptimizerCommands(): array
    {
        return [
            'pngquant',
            '--skip-if-larger',
            '--strip',
            '--quality=' . $this->imageQuality,
            '--ext=.png',
            '--force',
        ];
    }
}
