<?php

declare(strict_types=1);


namespace Csoft\Service\Image\Optimizer;


use Csoft\HttpFoundation\File\ImageFile;

interface ImageOptimizerInterface
{
    /**
     * Checks if the current optimizer instance is suitable to make image optimization.
     *
     * @param ImageFile $imageFile
     *
     * @return bool
     */
    public function isApplicable(ImageFile $imageFile): bool;

    /**
     * Optimizes the given image and returns the optimizer's script output.
     *
     * @param ImageFile $imageFile
     *
     * @return string
     */
    public function optimize(ImageFile $imageFile): string;
}
