# csoft/image-service
> This package is created to extends easy admin image upload capabilities.

## Main feaures  
- upload image
- optimize image
- create alternate sized versions

## Prerequisites
Optimization packages: pngquant, jpegoptim, gifsicle, svgo  
- you can install on linux `apt-get install jpegoptim pngquant gifsicle`
- svgo is coming from local node modules: `yarn add svgo` or `npm install svgo`

## Install
- initialize the service: `cp -R vendor/csoft/image-service/config .` 

## Usage
### Environment variable
#### Entity image path
>Images will be uploaded to `projectdir/public/image` with this config  
`ENTITY_IMAGE_PATH='image'`
#### Jpeg optimization quality
>The quality of the optimized image on a 1-100 scale  
`JPEG_QUALITY='80'`
#### Png optimization quality
>The quality of the optimized image on a 1-100 scale or a range  
>`PNG_QUALITY='65-80'`

### Make automatic imageupload for entities
You need to extend your entity from `AbstractEntityWithImage` or `AbstractEntityWithOptionalImage` abstract class

### Easyadmin configuration
```yaml
    Article:
      class: App\Entity\Article
      list:
        fields:
          - { property: 'imageFileName', label: 'Image', type: 'image' }
          - slug
          - title

      new:
        fields:
          - slug
          - { property: 'tempImageFileName', label: 'Image', type: 'file', type_options: { required: true } }
          - title
          - description

      edit:
        fields:
          - slug
          - { property: 'tempImageFileName', label: 'Image', type: 'file', type_options: { required: false } }
          - title
          - description
```

### Optimize uploaded images
Make your entity to implement `EntityWithOptimizedImagesInterface` interface or extend the `AbstractEntityWithOptimizedImage` or `AbstractEntityWithOptionalOptimizedImage` class.

### Generate resized versions
Make your entity to implement `EntityWithImageInMultipleSizesInterface` interface and implement the necessary method.
```php
use Csoft\Entity\AbstractEntityWithOptimizedImage;
use Csoft\Entity\EntityWithImageInMultipleSizesInterface;
use Csoft\Entity\ImageResizeDetails;

class MyEntity extends AbstractEntityWithOptimizedImage implements EntityWithImageInMultipleSizesInterface 
{
    /**
     * @inheritDoc
     */
    public function getImageResizeDetails(): array
    {
        return [
            /* 300x300 thumbnail goes to /thumbs folder */
            new ImageResizeDetails(300, 300),
            /* 500x500 thumbnail goes to /thumbs-big folder */
            new ImageResizeDetails(500, 500, 'thumbs-big'),
            /* 480w responsive version goes to /480w folder */
            new ImageResizeDetails(480, 0, '480w', false),
        ];
    }
}
```
#### CLI resized version generator
```bash
createResizedImage [crop|resize] [file name] [destination folder] [height] [width]
# Example:
createResizedImage crop /var/www/image/myfile.jpg /var/www/image/thumbs 300 300
```

### Twig usage
> Use your asset and take advantage of the alternate versions, lazy load or responsive images
```twig
<img data-src="{{ asset(article.imageFileName) }}" src="{{ asset(article.imageFileName | altPath('thumbs-big')) }}" alt="Super fancy article">
<img srcset="{{ asset(article.imageFileName | altPath('320w')) }},
             {{ asset(article.imageFileName | altPath('480w')) }} 1.5x,
             {{ asset(article.imageFileName) }} 2x"
     src="{{ asset(article.imageFileName) }}" alt="Super fancy article">
```

## Photo Album
### Photo entity
```php
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Csoft\Entity\Interfaces\PhotoAlbumInterface;
use Csoft\Entity\Interfaces\PhotoInterface;
use Csoft\Entity\Traits\PhotoTrait;

class Photo implements PhotoInterface
{
    use PhotoTrait;

    /**
     * @var PhotoAlbum the photo album this photo belongs
     * @ManyToOne(targetEntity="PhotoAlbum", inversedBy="photos")
     * @JoinColumn(name="photo_album_id", referencedColumnName="id")
     */
    protected $photoAlbum;


    public function getPhotoAlbum(): PhotoAlbumInterface
    {
        return $this->photoAlbum;
    }

    public function setPhotoAlbum(PhotoAlbumInterface $photoAlbum): self
    {
        $this->photoAlbum = $photoAlbum;

        return $this;
    }
}
```
### PhotoAlbum entity
```php
use Csoft\Collection\PhotoCollection;
use Doctrine\ORM\Mapping\OneToMany;
use Csoft\Entity\Interfaces\PhotoAlbumInterface;

class PhotoAlbum implements PhotoAlbumInterface
{
    /**
     * @var PhotoCollection
     * @OneToMany(targetEntity="Photo", mappedBy="photoAlbum")
     */
    protected $photos;

    public function __construct()
    {
        $this->photos = new PhotoCollection();
    }

    public function getPhotos(): ?PhotoCollection
    {
        return $this->photos;
    }

    public function setPhotos(?PhotoCollection $photos): self
    {
        $this->photos = $photos;

        return $this;
    }
}
```
