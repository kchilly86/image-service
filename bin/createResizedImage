#!/bin/sh
TYPE=$1
if [ -z "${TYPE}" ]; then
    echo '1. parameter, "type" needs to be specified (crop|resize)'
    exit 1
fi

FILE=$2
if [ -z "${FILE}" ]; then
    echo '2. parameter, "file" needs to be specified (/var/www/image/example.png)'
    exit 1
fi
FILE_NAME=$(basename "${FILE}")
if [ -z "${FILE_NAME}" ]; then
    echo '2. parameter, "file" needs to be a valid file!'
    exit 1
fi

DESTINATION_FOLDER=$3
if [ -z "${DESTINATION_FOLDER}" ]; then
    echo '3. parameter, "destination folder" needs to be specified (/var/www/image/thumbs)'
    exit 1
fi

IS_SQUARE=0

HEIGHT=$4
WIDTH=$5
if [ -z "${WIDTH}" ]; then
  if [ -z "${HEIGHT}" ]; then
    echo 'At least one of the 4. parameter, "height" and the 5. parameter "width" needs to be specified'
    exit 1
  fi
  IS_SQUARE=1
else
  if [ "${WIDTH}" = "${HEIGHT}" ]; then
    IS_SQUARE=1
  fi
fi

mkdir -p "${DESTINATION_FOLDER}"

# Crop image
if [ "${TYPE}" = 'crop' ]; then
    # Square crop
    if [ ${IS_SQUARE} = 1 ]; then
      MIN_DIMENSION=$(identify -format "%[fx:min(w,h)]" "${FILE}")
      CROP_WIDTH=${MIN_DIMENSION}
      CROP_HEIGHT=${MIN_DIMENSION}
    # Custom crop
    else
      IMAGE_WIDTH=$(identify -format "%w" "${FILE}")
      IMAGE_HEIGHT=$(identify -format "%h" "${FILE}")

      # Calculates the image size/crop size min ratio (width/width < or > height/height
      MIN_RATIO=$(php -r "echo(min(${IMAGE_WIDTH}/${WIDTH}, ${IMAGE_HEIGHT}/${HEIGHT}));")

      CROP_WIDTH=$(php -r "echo(round(${WIDTH}*${MIN_RATIO}));")
      CROP_HEIGHT=$(php -r "echo(round(${HEIGHT}*${MIN_RATIO}));")
    fi

    echo "Cropping file: ${FILE} (${CROP_WIDTH}x${CROP_HEIGHT})"
    convert "${FILE}" -gravity center -crop "${CROP_WIDTH}x${CROP_HEIGHT}"+0+0 +repage "${DESTINATION_FOLDER}/${FILE_NAME}"

    echo "Resizing cropped image: ${DESTINATION_FOLDER}/${FILE_NAME} (${WIDTH}x${HEIGHT})"
    magick mogrify -resize "${WIDTH}x${HEIGHT}" -quality 100 -path "${DESTINATION_FOLDER}" "${DESTINATION_FOLDER}/${FILE_NAME}"
# Resize image
else
    echo "Resizing image: ${FILE} (${WIDTH}x${HEIGHT})"
    magick mogrify -resize "${WIDTH}x${HEIGHT}" -quality 100 -path "${DESTINATION_FOLDER}" "${FILE}"
fi
